from datetime import timedelta
from django.http import HttpResponse
from django.shortcuts import redirect
import stripe
import os
from django.http.response import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import TemplateView
from dotenv import load_dotenv
from src.apps.account.models import UserDetail
from src.apps.payments.models import PaymentDetails, SubscriptionDetails

load_dotenv()


# stripe home page
class StripePurchasePage(TemplateView):
    template_name = 'templates/home.html'


# cancellation page render for stripe payment cancel condition
class CancellationConfirmed(TemplateView):
    template_name = 'templates/cancellation_confirmed.html'


# success page render for stripe payment success condition
class SuccessView(TemplateView):
    template_name = 'templates/success.html'

    def get(self, request, *args, **kwargs):
        session_id = request.GET["session_id"]
        payment_obj = PaymentDetails.objects.get(session_id=session_id)
        payment_obj.successed = True
        payment_obj.save()
        user_obj = UserDetail.objects.get(username=request.user)
        sub_end_date = user_obj.subscriptionEndAt + timedelta(days=30)
        user_obj.subscriptionEndAt = sub_end_date
        user_obj.save()
        try:
            sub_obj = SubscriptionDetails.objects.get(user=request.user)
            sub_obj.subscriptionEndDate = sub_end_date
            sub_obj.save()
        except:
            sub_obj = SubscriptionDetails.objects.create(
                user=request.user,
                status="RUNNING",
                subscriptionEndDate=sub_end_date
            )
            sub_obj.save()
        return redirect("/accounts/profile/")


# cancel page render for stripe payment cancel condition
class CancelledView(TemplateView):
    template_name = 'templates/cancelled.html'

    def get(self, request, *args, **kwargs):
        payment_obj = PaymentDetails.objects.filter(
            user=request.user).order_by("createdAt")[0]
        payment_obj.cancelled = True
        payment_obj.save()
        return redirect("/accounts/profile/")


# stripe config view function
@csrf_exempt
def stripe_config(request):
    if request.method == 'GET':
        stripe_config = {'publicKey': os.getenv(
            "STRIPE_PUBLISHABLE_KEY", default="")}
        return JsonResponse(stripe_config, safe=False)


# cancel subscription function
@csrf_exempt
def cancel_sub(request):
    try:
        user_obj = UserDetail.objects.get(username=request.user)
        sub_obj = SubscriptionDetails.objects.get(user=request.user)
        date = user_obj.createdAt + timedelta(days=7)
        sub_obj.subscriptionEndDate = date
        sub_obj.save()
        user_obj.subscriptionEndAt = date
        user_obj.save()
    except Exception as e:
        print(e)
    return redirect("/stripe/cancellation_confirmed/")


# Stripe checkout session function
@csrf_exempt
def create_checkout_session(request):
    if request.method == 'GET':
        domain_url = 'http://localhost:8000/'
        stripe.api_key = os.getenv("STRIPE_SECRET_KEY", default="")
        try:
            user = UserDetail.objects.get(username=request.user)
            checkout_session = stripe.checkout.Session.create(
                success_url=domain_url +
                'stripe/success?session_id={CHECKOUT_SESSION_ID}',
                cancel_url=domain_url + 'stripe/cancelled/',
                payment_method_types=['card'],
                mode='payment',
                line_items=[
                    {
                        'name': 'Trial Subscription',
                        'quantity': 1,
                        'currency': 'inr',
                        'amount': '350000',
                    }
                ]
            )
            payment = PaymentDetails.objects.create(
                user=user, session_id=checkout_session['id'])
            payment.save()
            return JsonResponse({'sessionId': checkout_session['id']})
        except Exception as e:
            return JsonResponse({'error': str(e)})


# Stripe webhook
# first start listening command in terminal "stripe listen --forward-to localhost:8000/webhook/""
@csrf_exempt
def stripe_webhook(request):
    stripe.api_key = os.getenv("STRIPE_SECRET_KEY", default="")
    endpoint_secret = os.getenv("STRIPE_ENDPOINT_SECRET", default="")
    payload = request.body
    sig_header = request.META['HTTP_STRIPE_SIGNATURE']
    event = None

    try:
        event = stripe.Webhook.construct_event(
            payload, sig_header, endpoint_secret
        )
    except ValueError as e:
        # Invalid payload
        return HttpResponse(status=400)
    except stripe.error.SignatureVerificationError as e:
        # Invalid signature
        return HttpResponse(status=400)

    # Handle the checkout.session.completed event
    if event['type'] == 'checkout.session.completed':
        print("Payment was successful.")
        # TODO: run some custom code here

    return HttpResponse(status=200)
