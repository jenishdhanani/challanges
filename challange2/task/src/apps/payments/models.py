from django.db import models
from src.apps.account.models import UserDetail

#payment details table to save the data of transaction created by user while purchasing subscription
class PaymentDetails(models.Model):

    paymentID = models.AutoField(primary_key=True, unique=True)
    user = models.ForeignKey(UserDetail, on_delete=models.CASCADE)
    successed = models.BooleanField(default=False, null=True, blank=True)
    cancelled = models.BooleanField(default=False, null=True, blank=True)
    session_id = models.TextField()

    createdAt = models.DateTimeField(auto_now_add=True)
    updatedAt = models.DateTimeField(auto_now=True)
    isDeleted = models.BooleanField(default=False, null=True)
    createdBy = models.CharField(max_length=120)
    updatedBy = models.CharField(max_length=120)

#subscription details table to save the data of subscription purchased by user after successful payment
class SubscriptionDetails(models.Model):
    
    subID = models.AutoField(primary_key=True, unique=True)
    user = models.ForeignKey(UserDetail, on_delete=models.CASCADE)
    status = models.CharField(max_length=120)
    subscriptionEndDate = models.DateTimeField(auto_now_add=True)

    createdAt = models.DateTimeField(auto_now_add=True)
    updatedAt = models.DateTimeField(auto_now=True)
    isDeleted = models.BooleanField(default=False, null=True)
    createdBy = models.CharField(max_length=120)
    updatedBy = models.CharField(max_length=120)