from django.urls import path

from .forms import CustomUserForm
from .views import *

from django_registration.backends.one_step.views import RegistrationView

#url patterns for user auth operations
urlpatterns = [
    path('login/', CustomLoginView.as_view(success_url='/accounts/profile/'), name='login'),
    # path('', include('django.contrib.auth.urls')),
    path('one_step_register/', RegistrationView.as_view(
            form_class=CustomUserForm,
            success_url='/accounts/profile/'),
            name='django_registration_register'),
    path('profile/', ProfileView.as_view(),
            name='profile_page',),
]
