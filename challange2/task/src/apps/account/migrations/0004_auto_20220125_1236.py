# Generated by Django 2.2.26 on 2022-01-25 07:06

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0003_auto_20220125_1230'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userdetail',
            name='subscriptionEndAt',
            field=models.DateTimeField(default=datetime.datetime(2022, 2, 1, 12, 35, 2, 506110)),
        ),
    ]
