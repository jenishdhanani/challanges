from datetime import datetime, timedelta
from django.db import models
from django.contrib.auth.models import AbstractUser

# custom user model to override the django registration AbstractUser model


class UserDetail(AbstractUser):

    userID = models.AutoField(primary_key=True, unique=True)
    username = models.CharField(max_length=128, null=False, unique=True)
    email = models.EmailField(null=False)
    useraddress = models.CharField(max_length=200, null=True, blank=True)
    password = models.CharField(max_length=255, null=False)
    subscriptionEndAt = models.DateTimeField(
        default=datetime.now()+timedelta(days=7))  # 7 days of trial will automatially added at registration time
    firstName = models.CharField(max_length=128, null=False, unique=True)
    lastName = models.CharField(max_length=128, null=False, unique=True)

    createdAt = models.DateTimeField(auto_now_add=True)
    updatedAt = models.DateTimeField(auto_now=True)
    isDeleted = models.BooleanField(default=False, null=True)
    isActivated = models.BooleanField(default=True, null=True)
    createdBy = models.CharField(max_length=120)
    updatedBy = models.CharField(max_length=120)
