fetch("/stripe/config/")
    .then((result) => { return result.json(); })
    .then((data) => {
        // Initialize Stripe Instance
        const stripe = Stripe(data.publicKey);
        // Event handler
        document.querySelector("#submitBtn").addEventListener("click", () => {
            // Get Checkout Session ID
            fetch("/stripe/checkout_session/")
                .then((result) => { return result.json(); })
                .then((data) => {
                    // Redirect to Stripe Checkout
                    return stripe.redirectToCheckout({ sessionId: data.sessionId })
                })
                .then((res) => {
                    console.log(res);
                });
        });
    });

fetch("/accounts/profile/")
    .then((result) => { return result.json(); })
    .then((data) => {
        // Initialize Stripe Instance
        const stripe = Stripe(data.publicKey);
        // Event handler
        document.querySelector("#submitBtn").addEventListener("click", () => {
            // Get Checkout Session ID
            fetch("/stripe/checkout_session/")
                .then((result) => { return result.json(); })
                .then((data) => {
                    // Redirect to Stripe Checkout
                    return stripe.redirectToCheckout({ sessionId: data.sessionId })
                })
                .then((res) => {
                    console.log(res);
                });
        });
    });
