# function defination
def compress(input_str=""):
    # initializing necessary variables
    n = len(input_str)
    output_str = ""
    count = 1  # atleast 1 count for every different characrter in string

    # check if input string has only one character then return string as it is
    if n == 1:
        return input_str

    # for loop that will iterate over the string
    for index in range(n):
        # save the character of latest iteration
        char = input_str[index]
        # compare character with next character in string and also check for the last iteration to prevent n+1 iteration
        if index+1 != n and char == input_str[index+1]:
            # count for the saved character will be increased if it is same as it's next character
            count += 1
        else:
            # save the collected data into the string if next character is different
            output_str += char + str(count)
            # initialize the count to 1 for new different character
            count = 1

    return output_str


print(compress(input_str="bbcceeee"))
print(compress(input_str="aaabbbcccaaa"))
print(compress(input_str="a"))
